package com.safebear.tasklist.usertests.page;

import lombok.NoArgsConstructor;
import org.openqa.selenium.WebDriver;

/**
 * Created by CCA_Student on 12/07/2018.
 */


// cb: This class needed creating - holy moly

    // CB: There are no static resources so there's is no way for me to be able to find out what the page displays as
    // so I guess you are asking for me to progress this as best possible.  Maybe....I'll see if I can get the build
    // to deploy to Jenkins.

    //CB: Okay so it's 2:40, I've built to jenkins at it' says it's fine
    //Sonarqube: Got no permissions to follow up static analysis of https://sonarcloud.io/api/ce/task?id=AWSOtv4NjBVAKLfOnGrB
    //API Tests have succeeded.
    //So, it's see how far i can get creating TaskListPage I guess.


    // Lets use the no args constructor to get the puppy up and runnning but I know I'll need to pass a driver

@NoArgsConstructor
public class TaskListPage {


    public void TaskListPage(WebDriver driver) {

    }

// this is to-do but without knowing what the UI looks like (code isn't in static folder it's meaningless unless we do
    // the TDD stuff instead of doing UI.
    public void a_user_creates_a_task(String taskName) {

    }

// and this is to do but we've not got a UI
    public boolean the_task_appears_in_the_list(String taskName) {
        return true ;

    }

}
