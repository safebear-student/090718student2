package com.safebear.tasklist.usertests;

import com.safebear.tasklist.model.Task;
import cucumber.api.DataTable;
import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Before;
import  com.safebear.tasklist.usertests.page.TaskListPage;
import org.junit.BeforeClass;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;



import java.util.List;

/**
 * Created by CCA_Student on 09/07/2018.
 * everything is awesome
 */



public class StepDefs {

    TaskListPage taskListPage ;
    WebDriver driver ;

    @Before
    public void setupApplication()  {


        driver = new ChromeDriver();
        driver.get("www.google.com");

        // Pass through the driver at this point - running out of time
        taskListPage = new TaskListPage();
    }


    public void StepDefs (){}

        // CB: At this point I am going to need to instantiate all the wonderful classes for selenium
        // I will then need to pass the driver through to my tasklistpage so that I can easily
        // use pagefactory to find elements.  However whether the UI I have no idea what I am looking for.
        //Darned autoimport - missed it :)

    @When("^a user creates a (.+)$")
    public void a_user_creates_a_task(String taskname) throws Throwable {
        // CB: remove pending exception and calling page object
        taskListPage.a_user_creates_a_task(taskname);

    }

    @Then("^the (.+) appears in the list$")
    public void the_task_appears_in_the_list(String taskname) throws Throwable {
        // CB: remove pending exception and calling page object
        taskListPage.the_task_appears_in_the_list(taskname);

    }


    //New stuff
    // CB Comment: The problem here is the need to bind task through the BDD statement.  Replcing the word task here with the regex
    // (.+) should solve the problem
     // No bind @Given("^the following (?:Configure Jenkins|Setup automation environment||Setup SQL Server) are created$")
    @Given("^the following tasks are created$")
    public void the_following_tasks_are_created(List <String> testData) throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        // For automatic transformation, change DataTable to one of
        // List<YourType>, List<List<E>>, List<Map<K,V>> or Map<K,V>.
        // E,K,V must be a scalar (String, Integer, Date, enum etc)
       // CB: The data passed in here is of a list of type string

        throw new PendingException();
    }

    @When("^the home page opens$")
    public void the_home_page_opens() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        throw new PendingException();
    }


        //@Then("^the following (?:'Configure Jenkins'|'Setup automation environment'||'Setup SQL Server') appear in the list:$")
    // CB Comment: the problem again is the minding of task using regex.  replacing the word task should solve this
    @Then("^the following (.+) appear in the list :$")
    public void the_following_tasks_appear_in_the_list(List testData) throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        // For automatic transformation, change DataTable to one of
        // List<YourType>, List<List<E>>, List<Map<K,V>> or Map<K,V>.
        // E,K,V must be a scalar (String, Integer, Date, enum etc)

        // CB : what is required here is a new class based around the Page Object Model

        throw new PendingException();
    }

    @Given("^a (.+) is in a (.+)$")
    public void a_task_is_in_a_state(String taskname, String status) throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        throw new PendingException();
    }

    @When("^a (.+) is updated to (.+)$")
    public void a_task_is_updated_to_newstate(String taskname, String newstate) throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        throw new PendingException();
    }

    @Then("^the (.+) is now in (.+)")
    public void the_task_is_now_in_newstate(String taskname, String newstate) throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        throw new PendingException();
    }
}
